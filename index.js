// Import thư viện Express Js
const { request } = require("express");
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;


let arr = ["Việt Nam", "Mỹ", "Ấn Độ"];

// Khai báo GET API trả ra simple message
app.get("/", (request, response) => {
    let today = new Date();
    let str = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    response.json({
        message: str
    })
});

// khai báo phương thức GET
app.get("/get-method", (req, res) => {
    res.json({
        arr: arr
    })
});

// Khai báo phương thức POST
app.post("/post-method", (req, res) => {
    res.json({
        arr: [...arr, "Canada"]
    })
});
// Khai báo API sử dụng request param
app.get("/request-params/:param", (request, response) => {
    const reques_param = request.params.param;

    response.json({
        result: arr[reques_param]
    })
});

// Khai báo API sử dụng request query
app.get("/request-query", (req, res) => {
    const query = req.query;

    const region = query.region;

    const find_region = arr.findIndex((item) => item == region);
    res.json({
        result: find_region
    })
});

// Khai báo để lấy đươc body-json
app.use(express.json());

// Khai báo API sử dụng body json
app.post("/request-body-json", (req, res) => {
    const body = req.body;

    res.json({
        result: body
    })
});

// Khai báo phương thức PUT
app.put("/put-method", (req, res) => {
    arr[3] = "Brazil";
    res.json({
        arr: arr
    })
});

// Khai báo phương thức DELETE
app.delete("/delete-method", (req, res) => {
    let [first, ...slice_arr] = arr
    res.json({
        arr: slice_arr
    })
});
// Chạy app trên cổng đã khai báo
app.listen(port, () => {
    console.log(`App đã chạy trên cổng ${port}`);
});
